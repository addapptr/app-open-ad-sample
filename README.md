# App Open Ad Sample Android

This app demonstrates open ads integration using AATKit.
See also the [wiki](https://aatkit.gitbook.io/android-integration/formats/appopen-google) for more instrucions.

---
## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@bitbucket.org:addapptr/app-open-ad-sample.git
```
---
## Maintainers
Current maintainers:

* [Damian Supera](https://bitbucket.org/damian_s/)
* [Michał Kapuściński](https://bitbucket.org/m_kapuscinski/)